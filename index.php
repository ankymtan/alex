<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="styles.css" rel="stylesheet" type="text/css">
</head>


<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

	<nav class="navbar navbar-default navbar-fixed-top" style="margin-top: 10px">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">Home</a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="#about">ABOUT</a></li>
	        <li><a href="#services">SERVICES</a></li>

          <?php
            if(isset($_COOKIE['login_name'])) {
              echo "<li><a href='me.php'><b>Welcome back, " . $_COOKIE['login_name'] . "</b></a></li>";
              echo '<a href="log-out.php"><button class="btn btn-lg btn-default">Log out</button></a>';
            } else {
              echo '<a href="log-in.php"><button class="btn btn-lg btn-default">Log in / Sign up</button></a>';
            }
          ?>
	      </ul>
	    </div>
	  </div>
	</nav>

	<div class="jumbotron text-center">
	  <h1>SGHome</h1>
	  <p>Post your room here for rent</p>
	  <a type="button" class="btn btn-default btn-lg" href="new-post.php">
      <span class="glyphicon glyphicon-plus"></span>
       Post
    </a>
	</div>

  <!-- Filter area code -->
  <div class="container">
    <Button class="btn btn-lg btn-default" style="float: right"
     data-toggle="collapse" data-target="#filters">
      Filter Posts
    </Button>

    <br />

    <form action="index.php" class="collapse form-horizontal" method="post" id="filters">
      <div class="form-group">
        <label class="control-label col-sm-2" for="email"></label>
        <div class="col-sm-8">
          <select class="form-control input-lg" required="true" name="location">

            <option disabled="true">-WEST AREA-</option>
            <option>Jurong West</option>
            <option>Joo Koon</option>
            <option>Clementi</option>
            <option>Bukit Batok</option>

            <option disabled="true"></option>
            <option disabled="true">-CENTER AREA-</option>
            <option>China Town</option>
            <option>Dhoby Ghaut</option>
            <option>Orchard</option>
            <option>City Hall</option>

            <option disabled="true"></option>
            <option disabled="true">-EAST AREA-</option>
            <option>Punggol</option>
            <option>Paya Lebar</option>
            <option>Stadium</option>
            <option>Bedok</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          <input type="number" required="true" class="form-control input-lg" name="price-from"
            placeholder="Rental fee from...">
        </div>
        <div class="col-sm-4">
          <input type="number" required="true" class="form-control input-lg" name="price-to"
            placeholder="To...">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          <input type="number" required="true" class="form-control input-lg" name="tenant"
            placeholder="Number of tenants">
        </div>
        <div class="col-sm-4">
          <input type="date" required="true" class="form-control input-lg" name="available-date"
            placeholder="Available from">
        </div>
      </div>

      <br />

      <center>
        <button type="submit" class="btn btn-warning btn-lg">Apply Filters</button>
      </center>
    </form>
  </div>

	<!-- Container (Pricing Section) -->
	<div id="pricing" class="container-fluid">
	  <div class="row">

		<?php
			$server = "localhost";
			$username = "root";
			$pass = "";
			$dbname = "Alex";

			// Create connection
			$conn = new mysqli($server, $username, $pass, $dbname);
			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			}

      if(empty($_POST['location'])){
        //if no filter is set
  			$sql = "SELECT * FROM post";
  			$result = $conn->query($sql);

  			if ($result->num_rows > 0) {
  			    // output data of each row

  			    while($row = $result->fetch_assoc()) {

  			    	echo "<div class='col-sm-4 col-xs-12'>
  					      <div class='panel panel-default text-center'>
  					        <div class='panel-heading'>
  					          <h1>" . $row['title'] . "</h1>
                      <h5 style='color: #666'> by <b>" . $row['poster_name'] . "</b></h5>
  					        </div>
  					        <div class='panel-body'>
  					          <h3><strong>" . $row['des'] . "</strong></h3>
  					        </div>
  					        <div class='panel-footer'>
  					          <h3>$" . $row['price'] . "</h3>
  					          <h4>per month</h4>
  					          <a href='post-detail.php?post_id=" . $row['id'] .
                      "'><button class='btn btn-lg'>Detail</button></a>
  					        </div>
  					      </div>
  					    </div>
  					    ";

  			    }
  			} else {
  			    echo "<h1 class='text-center'>No Post Yet</h1><br/><br/>";
  			}
      } else {
        //if the filter is set
        echo "<h4><center>" . $_POST['location'] . ", <br/>"
        . $_POST['tenant'] . " tenant(s) <br/>Price S$"
        . $_POST['price-from'] . "-"
        . $_POST['price-to'] . " <br />Available by "
        . $_POST['available-date'] . "</center></h4><br />";

        $sql = "SELECT * FROM post
          WHERE location = '" . $_POST['location'] . "' AND tenant = '"
          . $_POST['tenant'] . "' AND price > '"
          . $_POST['price-from'] . "' AND price < '"
          . $_POST['price-to'] . "' AND available_date <= '"
          . $_POST['available-date'] . "'";
  			$result = $conn->query($sql);

  			if ($result->num_rows > 0) {
  			  // output data of each row
			    while($row = $result->fetch_assoc()) {

			    	echo "<div class='col-sm-4 col-xs-12'>
					      <div class='panel panel-default text-center'>
					        <div class='panel-heading'>
					          <h1>" . $row['title'] . "</h1>
                    <h5 style='color: white'> by <b>" . $row['poster_name'] . "</b></h5>
					        </div>
					        <div class='panel-body'>
					          <h3><strong>" . $row['des'] . "</strong></h3>
					        </div>
					        <div class='panel-footer'>
					          <h3>$" . $row['price'] . "</h3>
					          <h4>per month</h4>
					          <a href='post-detail.php?post_id=" . $row['id'] .
                    "'><button class='btn btn-lg'>Detail</button></a>
					        </div>
					      </div>
					    </div>
					    ";
			    };
  			} else {
  			    echo "<h1 class='text-center'>No post match your filters.</h1><br/><br/>";
  			}
      }

			$conn->close();
		?>

	  </div>
	</div>

	<!-- Container (Contact Section) -->
	<div id="contact" class="container-fluid bg-grey">
	  <h2 class="text-center">CONTACT US</h2>
	  <div class="row">
	    <div class="col-sm-5">
	      <p>Contact us and we'll get back to you within 24 hours.</p>
	      <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
	      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
	      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
	    </div>
	  </div>
	</div>

	<footer class="container-fluid text-center">
	  <a href="#myPage" title="To Top">
	    <span class="glyphicon glyphicon-chevron-up"></span>
	  </a>
	</footer>

</body>
</html>
