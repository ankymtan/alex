<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Posted Successfully</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="styles.css" rel="stylesheet" type="text/css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background: #333">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>

<br/>
<br/>
<br/>

<!-- Main php area:  -->
  <?php

  $server = "localhost";
  $username = "root";
  $pass = "";
  $db = "Alex";

  // Create connection
  $connection = new mysqli($server, $username, $pass, $db);
  // Check connection
  if ($connection->connect_error) {
      die("Connection failed: " . $connection->connect_error);
  }

  $stmt = $connection->prepare("INSERT INTO post (title, des, price,
    poster_id, poster_name, location, tenant, available_date, pic)
  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

  $poster_name = $_COOKIE['login_name'];
  $poster_id = $_COOKIE['login_id'];

  $stmt->bind_param("ssiississ", $_POST['title'], $_POST['des'], $_POST['price'],
    $poster_id, $poster_name,
    $_POST['location'], $_POST['tenant'], $_POST['available-date'], $_POST['pic']);

  $stmt->execute();
  $stmt->close();


  $connection->close();

  ?>

<h2 class="text-center">Congratz, posted successfully!</h2>
<p class="text-center">Back home to see all the post</p>

<!-- Container (Pricing Section) -->
<div id="pricing" class="container-fluid">
  <div class="row">

    <div class="col-sm-12 col-xs-12">
      <div class="panel panel-default text-center">
        <div class="panel-heading">
          <h1>
            <?php
              echo $_POST["title"];
            ?>
          </h1>
        </div>
        <div class="panel-body">
          <p>
            <?php
              echo $_POST["des"];
            ?>
          </p>
          <?php

            echo "
            <center>
              <img src='" . $_POST['pic'] . "' class='img-rounded' width='500'>
            </center>"

          ?>
        </div>
        <div class="panel-footer">
          <h3>$

            <?php
              echo $_POST["price"];
            ?>

          </h3>
          <h4>per month</h4>
          <a class="btn btn-lg" href="index.php">Back Home</a>
        </div>
      </div>
    </div>


  </div>
</div>

<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
    </div>
    <div class="col-sm-7 slideanim">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit">Send</button>
        </div>
      </div>
    </div>
  </div>
</div>


<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Bootstrap Theme Made By <a href="http://www.w3schools.com" title="Visit w3schools">www.w3schools.com</a></p>
</footer>


</body>
</html>
