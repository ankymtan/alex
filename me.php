<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>My Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="styles.css" rel="stylesheet" type="text/css">
  <!-- bootbox.js at 4.4.0 -->
  <script src="https://rawgit.com/makeusabrew/bootbox/f3a04a57877cab071738de558581fbc91812dce9/bootbox.js"></script>
</head>


<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

	<nav class="navbar navbar-default navbar-fixed-top" style="background: transparent">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">Home</a>
	    </div>
	  </div>
	</nav>

	<div class="jumbotron text-center">
	  <h1>Hi <?php echo $_COOKIE['login_name']?>!</h1>
	  <p>This is your profile page.</p>
	</div>

	<!-- Container (Pricing Section) -->
	<div id="pricing" class="container-fluid">
	  <div class="row">

		<?php
			$server = "localhost";
			$username = "root";
			$pass = "";
			$dbname = "Alex";

			// Create connection
			$conn = new mysqli($server, $username, $pass, $dbname);
			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			}

			$sql = "SELECT * FROM post WHERE poster_id = " . $_COOKIE['login_id'];
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
          echo "<h1><center>Your posts</center</h1><br/><br/><br/>";
			    // output data of each row

			    while($row = $result->fetch_assoc()) {

			    	echo "<div class='col-sm-4 col-xs-12'>
					      <div class='panel panel-default text-center'>
					        <div class='panel-heading'>
					          <h1>" . $row['title'] . "</h1>
					        </div>
					        <div class='panel-body'>
					          <p><strong>" . $row['des'] . "</strong></p>
					        </div>
					        <div class='panel-footer'>
					          <h3>$" . $row['price'] . "</h3>
					          <h4>per month</h4>
					          <a href='post-detail.php?post_id=" . $row['id'] .
                    "'><button class='btn btn-lg'>Detail</button></a>
                    <button onclick='showConfirm(" . $row['id'] . ", \"" . $row['title'] . "\")' class='btn btn-lg'>Delete</button>
					        </div>
					      </div>
					    </div>
					    ";

			    }
			} else {
			    echo "<h1 class='text-center'>You Have No Post!</h1><br/><br/>";
			}

			$conn->close();
		?>

	  </div>
	</div>

	<footer class="container-fluid text-center">
	  <a href="#myPage" title="To Top">
	    <span class="glyphicon glyphicon-chevron-up"></span>
	  </a>
	</footer>

  <script>
    function showConfirm (id, title) {
      bootbox.confirm("Do you want to delete post \"" + title + "\"?",
        function(result){
          if(result){
            deletePost(id);
          }
        });
    }

    function deletePost (id) {
      window.location.href = "deletePost.php?id=" + id;
    }
  </script>

</body>
</html>
