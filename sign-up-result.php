<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Sign Up Success</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="styles.css" rel="stylesheet" type="text/css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Alex</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>


<!-- php file to create new account -->
<?php
$server = "localhost";
$username = "root";
$pass = "";
$db = "Alex";

//check if confirm password correct
if($_POST['pass'] != $_POST['pass-confirm']){
  die("<h1><center>Confirm password is not correct.</center></h1>");
}

// Create connection
$connection = new mysqli($server, $username, $pass, $db);
// Check connection
if ($connection->connect_error) {
  die("Connection failed: " . $connection->connect_error);
}

$stmt = $connection->prepare("INSERT INTO account (name, email, password)
VALUES (?, ?, ?)");

$stmt->bind_param("sss", $_POST['name'], $_POST['email'], $_POST['pass']);

$stmt->execute();
$stmt->close();

$connection->close();

//Sign Up Result
echo '<center style="margin-top: 20%">
  <h1>Hi ' . $_POST['name'] . ', your account is created successfully!</h1>
  <a href="log-in.php">
    <Button class="btn btn-lg btn-default">
      LOG IN
    </Button>
  </a>
</center>'

?>

</body>
</html>
