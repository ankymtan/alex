<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Log In</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="styles.css" rel="stylesheet" type="text/css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background: #333">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>



<!-- Sign Up -->
<div class="container text-center" style="margin-top: 15%; margin-bottom: 15%; border-radius: 10px; background: #DDD; padding: 40px; float: none; auto">

  <h1 class="text-center">Log In</h1>
  <br/>

  <?php
    // make sure this script only run when click 'submit',
    // not the first time page load
    if(!empty($_POST['email'])){

      $server = "localhost";
      $username = "root";
      $pass = "";
      $db = "Alex";

      // Create connection
      $connection = new mysqli($server, $username, $pass, $db);
      // Check connection
      if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
      }

      $sql = "SELECT id, name FROM account WHERE email = '" . $_POST['email']
        . "' && password = '" . $_POST['pass'] . "'";
      $result = $connection->query($sql);

      if($result->num_rows == 1){
        $row = $result->fetch_assoc();

        //password correct: set cookie and redirect to Home
        $cookie_name = "login_name";
        $cookie_value = $row['name'];
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

        //password correct: set cookie and redirect to Home
        $cookie_name = "login_id";
        $cookie_value = $row['id'];
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

        header("Location: index.php");
      }else{
        echo "Email / Password incorrect.";
      };

      $connection->close();

    }
  ?>

  <form action="log-in.php" method="post" class="form-horizontal">

    <div class="form-group">
      <label class="control-label col-sm-4">Email:</label>
      <div class="col-sm-4">
        <input type="email" required="true" class="form-control input-lg" name="email"></input>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4">Password</label>
      <div class="col-sm-4">
        <input type="password" required="true" class="form-control input-lg" name="pass">
      </div>
    </div>
      <center>
        <div class="form-group">
            <button type="submit" class="btn btn-default btn-lg" style="width: 100px">Log in
            </button>
        </div>
      </center>
      <p>Dont have an account?</p>

  </form>
    <a href="sign-up.php"><button type="submit"
      class="btn btn-warning btn-lg" style="width: 100px">Sign up</button>

</div>



</body>
</html>
