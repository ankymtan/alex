<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>New Post</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="styles.css" rel="stylesheet" type="text/css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background: #333">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>

<?php
  if(!isset($_COOKIE['login_name'])) {
    echo "<h1 style='margin-top: 15%;'><center>
    You need to log in before create a post.</center></h1>";
    die ('<a href="log-in.php"><center>
    <button class="btn btn-lg btn-default">Log in / Sign up</button></center></a>');
  }
?>

<!-- New Post -->
<div class="container text-center" style="margin-top: 15%; margin-bottom: 15%;
border-radius: 10px; background: #DDD; padding: 40px; float: none; auto">
  <h1 class="text-center">New Post</h1>
  <br/>

  <form action="new-post-success.php" method="post" class="form-horizontal">

    <div class="form-group">
      <label class="control-label col-sm-2" for="email"></label>
      <div class="col-sm-8">
        <input type="text" class="form-control input-lg" name="title"
          placeholder="Short title" required="true" >
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="email"></label>
      <div class="col-sm-8">
        <select class="form-control input-lg" name="location">

          <option disabled="true">-WEST AREA-</option>
          <option>Jurong West</option>
          <option>Joo Koon</option>
          <option>Clementi</option>
          <option>Bukit Batok</option>

          <option disabled="true"></option>
          <option disabled="true">-CENTER AREA-</option>
          <option>China Town</option>
          <option>Dhoby Ghaut</option>
          <option>Orchard</option>
          <option>City Hall</option>

          <option disabled="true"></option>
          <option disabled="true">-EAST AREA-</option>
          <option>Punggol</option>
          <option>Paya Lebar</option>
          <option>Stadium</option>
          <option>Bedok</option>
        </select>
      </div>
    </div>

    <br />

    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd"></label>
      <div class="col-sm-8">
        <input type="number" required="true" class="form-control input-lg"
          name="price"  placeholder="Rent per month">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2"></label>
      <div class="col-sm-8">
        <input type="number" class="form-control input-lg" name="tenant"
          required="true" placeholder="Room for how many tenants?">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2"></label>
      <div class="col-sm-8">
        <input type="date" class="form-control input-lg" name="available-date"
          required="true" placeholder="Room available from">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2"></label>
      <div class="col-sm-8">
        <input type="text" class="form-control input-lg" name="pic"
          required="true" placeholder="Link to the room picture">
      </div>
    </div>

    <br />

    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd"></label>
      <div class="col-sm-8">
        <textarea class="form-control input-lg" rows="5" name="des"
          required="true" placeholder="More room details..."></textarea>
      </div>
    </div>

    <br >

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-8">
        <button type="submit" class="btn btn-warning btn-lg">POST
        </button>
      </div>
    </div>

  </form>



</div>




<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
    </div>
  </div>
</div>


<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Bootstrap Theme Made By <a href="http://www.w3schools.com" title="Visit w3schools">www.w3schools.com</a></p>
</footer>


</body>
</html>
