<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Sign Up</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="styles.css" rel="stylesheet" type="text/css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background: #333">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>



<!-- Sign Up -->
<div class="container text-center" style="margin-top: 15%; margin-bottom: 15%; border-radius: 10px; background: #DDD; padding: 40px; float: none; auto">

  <h1 class="text-center">Sign Up New Account</h1>
  <br/>

  <form action="sign-up-result.php" method="post" class="form-horizontal">

    <div class="form-group">
      <label class="control-label col-sm-4">Name:</label>
      <div class="col-sm-4">
        <input type="text" required="true" class="form-control input-lg" name="name" placeholder="Your full name">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4">Email:</label>
      <div class="col-sm-4">
        <input type="email" required="true" class="form-control input-lg" name="email" placeholder="Most used email"></input>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4">Password</label>
      <div class="col-sm-4">
        <input type="password" required="true" class="form-control input-lg" name="pass"  placeholder="At least 8 character">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4">Confirm Password</label>
      <div class="col-sm-4">
        <input type="password" required="true" class="form-control input-lg" name="pass-confirm"  placeholder="Type the password again">
      </div>
    </div>

    <br />

    <div class="form-group">
      <center>
        <button type="submit" class="btn btn-default btn-lg">Sign up
        </button>
      </center>
    </div>

    <p>Already have an account?</p>

    <center>
      <a href="log-in.php"><button class="btn btn-warning btn-lg">Log in</button></a>
    </center>

  </form>

</div>



<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
    </div>
  </div>
</div>


<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Bootstrap Theme Made By <a href="http://www.w3schools.com" title="Visit w3schools">www.w3schools.com</a></p>
</footer>


</body>
</html>
