<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Post</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="styles.css" rel="stylesheet" type="text/css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background: #333">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>

<br/>
<br/>

<div class="container">
  <!-- Main php area:  -->
  <?php
    $server = "localhost";
    $username = "root";
    $pass = "";
    $db = "Alex";

    // Create connection
    $connection = new mysqli($server, $username, $pass, $db);
    // Check connection
    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }

    $sql = "SELECT * FROM post WHERE id = " . $_GET['post_id'];
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();

    echo
    "<br/><br/>
    <div>
      <div class='panel panel-default text-center'>
        <div class='panel-heading'>
          <h1>" . $row['title'] . "</h1>
          <h5 style='color: #666'> by <b>" . $row['poster_name']
          . " at " . $row['time'] . "</b></h5>
        </div>
        <div class='panel-body'>
          <h3><strong>" . $row['des'] . "</strong></h3>
          <h3><strong>Room at " . $row['location'] . " for " . $row['tenant']
          . " tenant(s). Available from " . $row['available_date'] .  "</strong></h3>
          <center>
          <img src='" . $row['pic'] . "' class='img-rounded' width='500'>
          </center>
        </div>
        <div class='panel-footer'>
          <h3>$" . $row['price'] . "</h3>
          <h4>per month</h4>
        </div>
      </div>
    </div>";

    $connection->close();
  ?>

  <!--If login then show the comment section-->
  <?php
    if(isset($_COOKIE['login_name'])) {
      echo
      '
      <!--New comment-->

        <form action="new-comment.php" method="post" class="form-inline">
          <div class="row">
            <div class="col-sm-10">
              <input type="text" required="true" class="form-control input-lg"
                name="comment" placeholder="Comment about this post..." style="width: 100%">
              <input class="hidden" type="text" name="post_id" value="' . $_GET['post_id'] . '">
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-danger btn-lg" style="width: 100%">Send</button>
            </div>
          </div>
        </form>
        <br/>
      ';
    }
  ?>

  <!--php to show all the comment-->
  <?php
    $server = "localhost";
    $username = "root";
    $pass = "";
    $dbname = "Alex";

    // Create connection
    $conn = new mysqli($server, $username, $pass, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT * FROM comment WHERE post_id = " . $_GET['post_id']
    . " ORDER BY time DESC";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
          echo
          "<div class='well well-sm' style='background: white; border: 1px solid #000'>
            <h4><kbd style='margin-right: 10px; margin-left: 10px'>"
            . $row['username'] . "</kbd>" . $row['content'] . "</h4>
          </div>";
        }
    } else {
      echo "<h3><center>No comment yet.</center></h3>";
    };
  ?>

</div>

<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Bootstrap Theme Made By <a href="http://www.w3schools.com" title="Visit w3schools">www.w3schools.com</a></p>
</footer>


</body>
</html>
